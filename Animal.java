package com.rafalborawski;

public abstract class Animal extends ObjectOnTheBoard{
    protected int indexOfTheAnimal;

    public Animal(String symbolOnTheBoard) {
        super(symbolOnTheBoard);
    }

    public Animal(String symbolOnTheBoard, int positionX, int positionY) {
        super(symbolOnTheBoard, positionX, positionY);
    }

    public String getInformationAboutPositionAfterMove() {
        return "(" + this.getPositionX() + "," + this.getPositionY() + ")";
    }

    @Override
    public boolean equals(Object obj) {

        boolean result = false;

        if (obj instanceof Animal) {
            if (this.getPositionX() == ((Animal)obj).getPositionX() && this.getPositionY() == ((Animal)obj).getPositionY()) {
                result = true;
            }
        }
        return result;
    }

    public int getIndexOfTheAnimal() {
        return indexOfTheAnimal;
    }

}
