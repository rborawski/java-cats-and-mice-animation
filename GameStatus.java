package com.rafalborawski;

public enum GameStatus {
    catsWon, miceWon, draw, stillPlaying
}
