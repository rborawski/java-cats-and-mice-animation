package com.rafalborawski;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AnimationBoard {

    private static final int BOARD_SIZE = 9;

    private static final int MAX_MICE = 1;
    private static final int MAX_CATS = 10;
    private static final int DRAW_BOARD_PERIOD = 1;

    private int middleOfTheBoard = BOARD_SIZE / 2;
    private int mouseAlive = MAX_MICE;
    private int roundNumber = 1;

    private List<List<ObjectOnTheBoard>> board;
    private List<Cat> cats;
    private List<Mouse> mice;

    public AnimationBoard() {
        this.board = new ArrayList<>();
        this.cats = new ArrayList<>();
        this.mice = new ArrayList<>();

        this.createEmptyBoard();
        this.createAnimals();
        this.animalInsertIntoBoard();
    }

    private void createEmptyBoard() { // 1

        for (int i = 0; i < BOARD_SIZE; i++) {
            this.board.add(new ArrayList<>());
            for (int j = 0; j < BOARD_SIZE; j++) {
                if ((i == middleOfTheBoard) && (j == middleOfTheBoard)) {
                    this.board.get(i).add(new Cheese(i, j));// ustawia pozycje startowa sera na srodek/srodek planszy

                } else {
                    this.board.get(i).add(new EmptyObject()); // wstawia '.'
                }
            }
        }

    }

    private void clearCurrentBoard() {

        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if ((i == middleOfTheBoard) && (j == middleOfTheBoard)) {
                    this.board.get(i).set(j, new Cheese(i, j));// ustawia pozycje startowa sera na srodek/srodek planszy

                } else {
                    this.board.get(i).set(j, new EmptyObject()); // wstawia '.'
                }
            }
        }
    }

    private void createAnimals(){ // 2

        while (this.mice.size() < MAX_MICE){
            int x = generateRandomNumber(); // losowanie pozycji X obiektu
            int y = 0;

            if ((x == 0) || (x == (BOARD_SIZE - 1))) {
                y = generateRandomNumber(); // losowanie Y
            } else {
                int z = generateRandomNumber(2); // pomocnicza wartosc, 0 lub 1 zeby zadecydowac czy Y ma miec wartosc 0 lub board_size - 1
                if (z == 0) {
                    y = 0;
                } else if (z == 1) {
                    y = (BOARD_SIZE - 1);
                }
            }

            //if (isMouseOnTheList(x, y) == false) {
            // this.mice.add(new Mouse(x, y));
            //}

            if (hasAnimalTheSamePositionDuringSpawn(x, y, this.mice) == false) {
                this.mice.add(new Mouse(x, y));
            }


        }

        while ((this.cats.size() < MAX_CATS)) {
            int x = generateRandomNumber(); // losowanie pozycji X obiektu
            int y = generateRandomNumber(); // losowanie pozycji Y obiektu

            //if (isMouseOnTheList(x, y) == false && isCatOnTheList(x, y) == false) {
            //this.cats.add(new Cat(x, y));
            //}

            if (hasAnimalTheSamePositionDuringSpawn(x, y, this.mice) == false && hasAnimalTheSamePositionDuringSpawn(x, y, this.cats) == false) {
                this.cats.add(new Cat(x, y));
            }
        }
    }

    private int generateRandomNumber(int rangeOfRandomNumber) { // generuje liczbe do wpisanego zakresu
        return (int)(Math.random() * 1000) % rangeOfRandomNumber;
    }

    private int generateRandomNumber() { // generuje liczbe do board_size, uzyte przy tworzeniu pierwszych zwierzat
        return this.generateRandomNumber(BOARD_SIZE); //int x = (int)Math.random() * 1000 % 8; //random od 0 do 7
    }

    private int generateAnimalMoveNumber() { // X,Y dodac -1,0,1, do losowanie ruchu zwierzat
        return this.generateRandomNumber(3) - 1;
    }

    private void animalMove() { // 5
        this.roundNumber++;
        this.miceMove();
        this.catsMove();
    }

    private void miceMove() {
        for (Mouse mouse : this.mice) {
            if (mouse.isAlive()) {
                int x = generateAnimalMoveNumber();
                int y = generateAnimalMoveNumber();

                if (x == 0 && y == 0) {
                    do {
                        y = generateAnimalMoveNumber();
                    } while (y == 0);
                }

                while ((mouse.getPositionX() + x < 0) || (mouse.getPositionX() + x == BOARD_SIZE) || (mouse.getPositionY() + y < 0) || (mouse.getPositionY() + y == BOARD_SIZE)) {
                    x = generateAnimalMoveNumber();
                    y = generateAnimalMoveNumber();

                    if (x == 0 && y == 0) {
                        do {
                            y = generateAnimalMoveNumber();
                        } while (y == 0);
                    }
                }

                this.printAnimalMove(mouse, mouse, x, y);
            }

        }
    }

    private void catsMove() {
        for (Cat cat : this.cats) {

            int x = generateAnimalMoveNumber();
            int y; // nie potrzebne losowanie

            if (x == 0) {
                do {
                    y = generateAnimalMoveNumber();
                } while (y == 0);
            } else {
                y = 0;
            }

            while ((cat.getPositionX() + x < 0) || (cat.getPositionX() + x == BOARD_SIZE) || (cat.getPositionY() + y < 0) || (cat.getPositionY() + y == BOARD_SIZE)) {
                x = generateAnimalMoveNumber();

                if (x == 0) {
                    do {
                        y = generateAnimalMoveNumber();
                    } while (y == 0);
                } else {
                    y = 0;
                }
            }

            this.printAnimalMove(cat, cat, x, y); // polimorfizm
        }
    }

    private void printAnimalMove(InformatingAboutAnimalPosition animalInterface, Animal animal, int x, int y) { // polimorfizm

        String moveInformation = animalInterface.getInformationAboutPositionBeforeMove();

        animal.setPositionX(animal.getPositionX() + x);
        animal.setPositionY(animal.getPositionY() + y);

        moveInformation += animal.getInformationAboutPositionAfterMove();

        System.out.println(moveInformation);
    }

    private boolean hasAnimalTheSamePositionDuringSpawn(int x, int y, List<? extends Animal> animals) {
        for (Animal animal : animals) {
            if (x == animal.getPositionX() && y == animal.getPositionY() || x == this.middleOfTheBoard && y == this.middleOfTheBoard) { // czy jest na animal przy spawnie i czy nie jest na serze
                return true;
            }
        }
        return false;
    }

//    private boolean isMouseOnTheList(int x, int y) {
//
//        for (Mouse mouse : this.mice) {
//            if (x == mouse.getPositionX() && y == mouse.getPositionY()) { // czy jest na myszy przy spawnie
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private boolean isCatOnTheList(int x, int y) {
//
//        for (Cat cat : this.cats) {
//            if (x == cat.getPositionX() && y == cat.getPositionY() || x == this.middleOfTheBoard && y == this.middleOfTheBoard) { // czy jest na jakims kocie i czy jest na serze przy spawnie
//                return true;
//            }
//        }
//        return false;
//    }

    private void drawBoard() { // 4

        for (int i = 0; i < this.board.size(); i++) {

            for (int j = 0; j < this.board.get(i).size(); j++){
                System.out.print(this.board.get(j).get(i).getSymbolOnTheBoard());
            }
            System.out.println();
        }
    }

    private void animalInsertIntoBoard() { // 6

        this.clearCurrentBoard(); // zeruje cala tablice i nizej na nowo uzupelnia

        this.checkIfMouseWasCaught();

        for (Mouse mouse : this.mice) {
            if (mouse.isAlive()) {
                this.board.get(mouse.getPositionX()).set(mouse.getPositionY(), mouse);
            }
        }

        for (Cat cat : this.cats) {
            this.board.get(cat.getPositionX()).set(cat.getPositionY(), cat);
        }

        this.board.get(this.middleOfTheBoard).set(this.middleOfTheBoard, new Cheese(this.middleOfTheBoard, this.middleOfTheBoard)); // czy ser zawsze musi byc na wierzchu ?

    }

    private void checkIfMouseWasCaught() {
        for (Mouse mouse : this.mice) {
            if (mouse.isAlive()) {
                for (Cat cat : this.cats) {
                    if (mouse.equals(cat)) {
                        if (this.mouseAlive == 1 && mouse.getPositionX() == this.middleOfTheBoard && mouse.getPositionY() == this.middleOfTheBoard) {
                            continue;
                        }
                        else {
                            System.out.println("Kot " + cat.getIndexOfTheAnimal() + " zlapal Mysz " + mouse.getIndexOfTheAnimal());
                            if (mouse.isAlive()) {
                                mouse.setAlive(false);
                                this.mouseAlive--;
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean isCatWinner() {
        boolean result = false;

        if (this.mouseAlive == 0) {
            result = true;
        }
        return result;
    }

    private boolean isMouseWinner() {
        boolean result = false;

        for (Mouse mouse : this.mice) {
            if (mouse.isAlive()) {
                if (mouse.getPositionX() == this.middleOfTheBoard && mouse.getPositionY() == this.middleOfTheBoard) {
                    System.out.println("Mysz " + mouse.getIndexOfTheAnimal() + " dotarla do sera!");
                    result = true;
                }
            }
        }

        return  result;
    }

    private boolean isDraw() {
        boolean result = false;

        for (Mouse mouse : this.mice) {
            if (mouse.isAlive()) {
                for (Cat cat : this.cats) {
                    if (mouse.equals(cat)) {
                        if (this.mouseAlive == 1 && mouse.getPositionX() == this.middleOfTheBoard && mouse.getPositionY() == this.middleOfTheBoard) {
                            System.out.println("Ostatnia Mysz " + mouse.getIndexOfTheAnimal() + " dotarla do sera wraz z Kotem " + cat.getIndexOfTheAnimal());
                            result = true;
                        }
                    }
                }
            }

        }

        return result;
    }

    private GameStatus checkResults() { // 7

        if (isCatWinner()) {
            System.out.println("Koty wygraly!");
            return GameStatus.catsWon;
        }

        if (isDraw()) {
            System.out.println("Remis!");
            return GameStatus.draw;
        }

        if (isMouseWinner()) {
            System.out.println("Myszy wygraly!");
            return GameStatus.miceWon;
        }

        return GameStatus.stillPlaying;
    }

    public void start() {

        JavaGraphics javaGraphics = new JavaGraphics();

        GameStatus gameStatus = GameStatus.stillPlaying;

        while (gameStatus == GameStatus.stillPlaying) {

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //System.out.println("*************************");

            System.out.println("Runda " + this.roundNumber + ", pozostalo " + this.mouseAlive + " myszy");

            if (this.roundNumber == 1 || this.roundNumber % DRAW_BOARD_PERIOD == 0) {
                this.drawBoard();
            }

            javaGraphics.redrawContent();

            this.animalMove();
            this.animalInsertIntoBoard();

            gameStatus = this.checkResults();
        }

        javaGraphics.redrawContent();
        this.drawBoard();

        //System.out.println("*************************");
    }

///////////////////////////////////////////////////////////////
    private class JavaGraphics extends JFrame {

        DrawCanvas drawCanvas = new DrawCanvas();

        public JavaGraphics() throws HeadlessException {
            setSize(800, 800);
            setLocationRelativeTo(null);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setVisible(true);
            setContentPane(drawCanvas);
        }

        public void redrawContent() {

            DrawCanvas drawCanvas2 = new DrawCanvas();

            drawCanvas2.setBackground(Color.BLACK);
            setVisible(true);

            setContentPane(drawCanvas2);

        }
    }

    private class DrawCanvas extends JPanel {

        public DrawCanvas() {

        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            setBackground(Color.BLACK);
            g.setColor(Color.BLUE);

            int x = 50;
            int y = 50;
            int widthAndHeight = 25;

            for (int i = 0; i < board.size(); i++) {

                for (int j = 0; j < board.get(i).size(); j++){

                    if (board.get(i).get(j).getSymbolOnTheBoard().equals(".")) {
                        g.setColor(Color.BLUE);
                        g.drawRect(x, y, widthAndHeight, widthAndHeight);
                    } else if (board.get(i).get(j).getSymbolOnTheBoard().equals("M")) {
                        g.setColor(Color.GRAY);
                        g.drawRect(x, y, widthAndHeight, widthAndHeight);
                        g.fillRect(x, y, widthAndHeight, widthAndHeight);
                    } else if (board.get(i).get(j).getSymbolOnTheBoard().equals("K")) {
                        g.setColor(Color.RED);
                        g.drawRect(x, y, widthAndHeight, widthAndHeight);
                        g.fillRect(x, y, widthAndHeight, widthAndHeight);
                    } else if (board.get(i).get(j).getSymbolOnTheBoard().equals("S")) {
                        g.setColor(Color.YELLOW);
                        g.drawRect(x, y, widthAndHeight, widthAndHeight);
                        g.fillRect(x, y, widthAndHeight, widthAndHeight);
                    }

                    y += 50;
                }

                y = 50;
                x += 50;
            }
        }

    }
  ///////////////////////////////////////////////////////////////////////
}

