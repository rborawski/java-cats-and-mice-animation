package com.rafalborawski;

public class Cheese extends ObjectOnTheBoard {

    public Cheese(int positionX, int positionY) {
        super("S", positionX, positionY);
    }
}
