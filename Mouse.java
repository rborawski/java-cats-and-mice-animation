package com.rafalborawski;

public class Mouse extends Animal implements InformatingAboutAnimalPosition {

    private static int counterOfTheMouse = 1;
    private boolean isAlive;

    public Mouse(int positionX, int positionY) {
        super("M", positionX, positionY);
        this.indexOfTheAnimal = counterOfTheMouse++;
        this.isAlive = true;
    }

    @Override
    public String getInformationAboutPositionBeforeMove() {
        return "Mysz " + this.getIndexOfTheAnimal() + ": " + this.getInformationAboutPositionAfterMove() + " -> ";
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

}
