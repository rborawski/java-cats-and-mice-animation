package com.rafalborawski;

public interface InformatingAboutAnimalPosition {

    String getInformationAboutPositionBeforeMove();
}
