package com.rafalborawski;

public abstract class ObjectOnTheBoard {

    private String symbolOnTheBoard;
    private int positionX;
    private int positionY;

    public ObjectOnTheBoard(String symbolOnTheBoard) {
        this.symbolOnTheBoard = symbolOnTheBoard;
    }

    public ObjectOnTheBoard(String symbolOnTheBoard, int positionX, int positionY) {
        this(symbolOnTheBoard);
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public String getSymbolOnTheBoard() {
        return symbolOnTheBoard;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }
}
