package com.rafalborawski;

public class Cat extends Animal implements InformatingAboutAnimalPosition {

    private static int counterOfTheCat = 1;

    public Cat(int positionX, int positionY) {
        super("K", positionX, positionY);
        this.indexOfTheAnimal = counterOfTheCat++;
    }

    @Override
    public String getInformationAboutPositionBeforeMove() {
        return "Kot " + this.getIndexOfTheAnimal() + ": " + this.getInformationAboutPositionAfterMove() + " -> ";
    }

}
